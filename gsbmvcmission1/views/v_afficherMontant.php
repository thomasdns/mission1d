<h3>Fiche de frais par type: </h3>
  <div class="encadre">	
  	<table class="listeLegere">
  	    <caption>Cumul pour tous les mois des frais au forfait par poste</caption>
          <tr>
            <th class="libelle">Repas midi</th>
            <th class='libelle'>Nuitée</th> 
            <th class="libelle">Etape </th>
            <th class='libelle'>Km</th>               
          </tr>
      <?php      
      foreach ($lesInfosFicheFrais as $fraisTotauxParType) 
		  {
        if($fraisTotauxParType['idFraisForfait']  == 'REP')
        { 
          $rep = $fraisTotauxParType['total'];
        }
        elseif($fraisTotauxParType['idFraisForfait']  == 'NUI')
        { 
          $nui = $fraisTotauxParType['total'];
        }
        elseif($fraisTotauxParType['idFraisForfait']  == 'ETP')
        { 
          $etp = $fraisTotauxParType['total'];
        }
        elseif($fraisTotauxParType['idFraisForfait']  == 'KM')
        { 
          $km = $fraisTotauxParType['total'];
        }
      } 
      $repPourcent = $rep/($rep+$nui+$etp+$km)*100;
      $nuiPourcent = $nui/($rep+$nui+$etp+$km)*100;
      $etpPourcent = $etp/($rep+$nui+$etp+$km)*100;
      $kmPourcent = $km/($rep+$nui+$etp+$km)*100;
		  ?>
        <tr>
          <td><?php echo $rep ?></td>
          <td><?php echo $nui ?></td>
          <td><?php echo $etp ?></td>
          <td><?php echo $km ?></td>
        </tr>
        <tr>
        <td><?php echo $repPourcent."%" ?></td>
          <td><?php echo $nuiPourcent."%" ?></td>
          <td><?php echo $etpPourcent."%" ?></td>
          <td><?php echo $kmPourcent."%" ?></td>
        </tr>
    </table>
  </div>